# Overview

To complete this challenge, you will need to create a simple React web app, and provide us with the following:

- **Source files** - ZIP or Github link
- **Public URL** - Working output of the coding challenge (hosted page)

The objective of this challenge is to guage your skills and approach to building a simple web app, given a set of screens and an API feed. We will also look at the generated HTML, CSS and JS output.

# The Test

Using the provided screens as a reference, you'll need to build a basic React single page application style application with history support (https://developer.mozilla.org/en-US/docs/Web/API/History) for the 2 different pages/routes by fetching the local JSON feed which you'll use to render the content on the page. Basic sorting UI and functionaity will also be need to be included on the detail page.

Although this is a basic exercise, we'll be looking for simple, performant, well-designed and tested code.

**Please also include a README with any setup instructions, and any tests or other documentation you created and is required to run and view your application**

# Details and Requirements

You will need to build the following pages and functionality;

-   Listing Page
-   'Single Item' Detail Page
    -   Toggleable sorting functionality for sorting the questions on the detail page by ascending/descending date using the `date` key in each of the question objects

### Tech and Language

-   Ideally write and take advantage of ES6/ECMAScript 2015 features (transpiling with Babel (https://babeljs.io/) for wider browser support if possible)
-   If possible write and transpile to CSS with SASS/SCSS (https://sass-lang.com/)

### Data

A supplied JSON file - **/feed/data.json** - is included in the Bitbucket repository. You will need to use this data to render the page content.

### Design

Attention to detail when creating and styling components from the sketch design will be noted, we would however also like to see consideration and thought in regards responsive design and mobile specific layout where possible (content stacking, mobile menu, etc).

### Designs

The Figma file can be found in https://www.figma.com/file/pIEaWWbngjVDo0wxZ8PKyv/FED-Interview-Test?node-id=0%3A1

### Fonts

Fonts should be available freely and setup in the test using google web fonts hosting (https://fonts.google.com/)
